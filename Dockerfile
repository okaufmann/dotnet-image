FROM microsoft/dotnet:2.1-sdk-alpine

### -------- Common packages ----------- ###
# Install .NET CLI dependencies
RUN apk add --no-cache git zip unzip gnupg

### ------- GIT --------------------- ###
ADD .gitconfig /root/.gitconfig
RUN git config --global user.name "Default Dev"
RUN git config --global user.email git@mot.ch

WORKDIR /app
